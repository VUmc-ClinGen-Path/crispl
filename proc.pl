#!/usr/bin/perl
$| = 1;
my $tko = shift;
open TKO, "<$tko";
my @fqs = @ARGV;

my %data = ();
while (<TKO>) {
	chomp;
	my @row = split(/\t/, $_);
	my @ann = split(/\:/, $row[2]);
	$data{$row[0]}{data} = [@row];
	$data{$row[0]}{symbol} = $ann[0];
	$data{$row[0]}{cnt} = [(0) x scalar(@fqs)];
}
close TKO;
my @fqbn = ();
foreach my $i (0 .. $#fqs) {
	my $fq = $fqs[$i];
	my $name = `basename $fq`;
	chomp $name;
	push @fqbn, $name;
	print STDERR $fq . "\n"; sleep 1;
	open FQ, "zcat $fq |";
	while (<FQ>) {
		my $seq = readline FQ;
		readline FQ;
		readline FQ;
		chomp $seq;
		$seq = substr($seq, 21, 20);
		if (not exists($data{$seq})) {
	#		next;
			$data{$seq}{data} = ["Unknown"];
			$data{$seq}{symbol} = "Unknown";
			$data{$seq}{cnt} = [(0) x scalar(@fqs)];
		}
		$data{$seq}{cnt}[$i]++;
	}
	close FQ;
}

print join("\t", "gRNA", "Symbol", @fqbn) . "\n";
foreach my $seq (sort keys(%data)) {
	print join("\t", $seq, $data{$seq}{symbol}, @{$data{$seq}{cnt}}) . "\n";
}
