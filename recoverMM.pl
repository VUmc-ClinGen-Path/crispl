#!/usr/bin/perl
$| = 1;
my $tko = shift;
my $counts = shift;
my $minCnt = shift || 50;
open TKO, "<$tko";
my %data = ();
while (<TKO>) {
	chomp;
	my @row = split(/\t/, $_);
	my @ann = split(/\:/, $row[2]);
	$data{$row[0]}{data} = [@row];
	$data{$row[0]}{symbol} = $ann[0];
	$data{$row[0]}{cnt} = [];
}
close TKO;

open COUNTS, "<$counts";
my $hdr = readline(COUNTS);
while(<COUNTS>) {
	chomp;
	my @row = split(/\t/, $_);
	
	if (exists($data{$row[0]})) {
		$data{$row[0]}{symbol} = $row[1];
		foreach my $i (0 .. $#row - 2) {
			$data{$row[0]}{cnt}->[$i] += $row[$i + 2];
		}
	}
	else {
		my $totCnt = 0;
		$totCnt += $_ foreach @row[2 .. $#row];
		my $avgCnt = $totCnt / (scalar(@row) - 2);
		if ($avgCnt >= $minCnt) {
			print STDERR "Attempting $row[0] : $avgCnt\n";
			my @match = &permute($row[0]);
			foreach my $m (@match) {
				foreach my $i (0 .. $#row - 2) {
					$data{$m}{cnt}->[$i] += $row[$i + 2];
				}
			}
			if (scalar(@match) > 1) {
				print STDERR "Warning, multiple recoveries!\n"; sleep 1;
			}	

		}	
	}
}

print $hdr;
#print join("\t", "gRNA", "Symbol", @fqbn) . "\n";
foreach my $seq (sort keys(%data)) {
	print join("\t", $seq, $data{$seq}{symbol}, @{$data{$seq}{cnt}}) . "\n";
}

sub permute {
	my $seq = shift;
	my @match = ();
	foreach my $i (0 .. length($seq) - 1) {
		my $pseq = $seq;
		foreach my $nt (qw/A C G T/) {
			substr($pseq, $i, 1, $nt);
			if (exists($data{$pseq})) {
				print STDERR "Recovery $pseq <-> $seq\n";
			#	sleep 1;
				push @match, $pseq;	
			}
		}
#		print STDERR "."
	}
#	print STDERR join(":", @match) . "\n"; sleep 1;
	return(@match);
}
